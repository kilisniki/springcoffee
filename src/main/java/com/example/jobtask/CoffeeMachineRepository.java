package com.example.jobtask;

import org.springframework.data.repository.CrudRepository;


public interface CoffeeMachineRepository extends CrudRepository<CoffeeMachine, Long> {
}