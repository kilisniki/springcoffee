package com.example.jobtask;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
public class ApiController {

    final CoffeeMachineRepository coffeeMachineRepository;
    private CoffeeMachine coffeeMachine = new CoffeeMachine();

    public ApiController(CoffeeMachineRepository coffeeMachineRepository) {
        this.coffeeMachineRepository = coffeeMachineRepository;
        coffeeMachineRepository.save(coffeeMachine);
    }

    @GetMapping("/info")
    public Object getVisits() {
        coffeeMachineRepository.save(coffeeMachine);
        return coffeeMachine;
    }

    @GetMapping("/getCoffee")
    public Object getCoffee() {
        try {
            return coffeeMachine.makeCoffee();
        } catch (Exception e){
            return new ErrorMessage(e.getMessage(), 1);
        }
    }

    @GetMapping("/push")
    public Object push(@RequestParam("w") int water, @RequestParam("m") int milk, @RequestParam("b") int beans){
        try {
            coffeeMachine.push(water, milk, beans);
        } catch (Exception e){
            return new ErrorMessage(e.getMessage(), 2);
        }
        return coffeeMachine;
    }

    @GetMapping("/clean")
    public Object clean(){
        coffeeMachine.clean();
        return coffeeMachine;
    }
}