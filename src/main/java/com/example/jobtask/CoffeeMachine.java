package com.example.jobtask;



import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;


@Entity
public class CoffeeMachine {

    @Id
    @GeneratedValue
    public Long id;

    public int milk;
    public int water;
    public int beans;
    public int workcount;

    public CoffeeMachine(){
        milk = 0;
        water = 0;
        beans = 0;
        workcount = 0;
    }

    public Coffee makeCoffee()throws Exception{
        if(milk<100)
            throw  new Exception("Error, not enough milk.");
        if(water<150)
            throw  new Exception("Error, not enough water.");
        if(beans<15)
            throw  new Exception("Error, not enough beans.");
        if(workcount>=8)
            throw  new Exception("Error, need cleaning");

        milk -= 100;
        water -= 150;
        beans -= 15;
        workcount++;

        return new Coffee();
    }

    public void push(int water, int milk, int beans) throws Exception{
        if(this.water+water>2000){
            throw new Exception("Error: overflow of water");
        }
        if(this.milk+milk>1000){
            throw new Exception("Error: overflow of milk");
        }
        if(this.beans+beans>250){
            throw new Exception("Error: overflow of beans");
        }

        this.water += water;
        this.milk += milk;
        this.beans += beans;

    }

    public void clean(){
        workcount = 0;
    }




}