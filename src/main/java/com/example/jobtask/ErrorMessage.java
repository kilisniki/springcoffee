package com.example.jobtask;

public class ErrorMessage {
    public String text;
    public int code;

    public ErrorMessage(String text, int code){
        this.text = text;
        this.code = code;
    }
}
