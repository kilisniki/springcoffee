# SpringCoffee

API Coffe Machine with Spring Boot

Need JDK 8.
routes:
<br>
../api/info - information of coffe machine<br>
../api/getCoffee - get coffee<br>
../api/push?w=1&m=100&b=50 - push 1ml of Water, 100ml of Milk and 50mg coffee beans.<br>
../api/clean - cleaning machine<br>

May be work on http://undegr.ru:8080/api/info